<?php include "connect.php" ?>
<?php
	$event_id = $_GET['Tid'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Events</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="events.css">
	<link href="https://fonts.googleapis.com/css?family=Chakra+Petch" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="EventDescription.css">
</head>
<body class="container-fluid">
		<div style="width: 70% ;float: left;">
			<h1 class="eventsHead" style="width: 100%;">EVENTS</h1>
		</div>
		<div style="width: 30%;float: right">
			<img src="Cult.png">
		</div>
		<hr>
		<br><br>
		<br>
		<div style="width: 60%; font-size: 30%; ">
			<h1 class="eventsSubHead" style="width: 100%;">
					<?php $sql= "SELECT TName from TEvents where Tno=$event_id;";
					  $result = $conn->query($sql);
					if ($result->num_rows > 0) {
						while ($row = $result->fetch_assoc()) {
				    	echo $row['TName'];
						}
					}
					else {
						echo 'No Result';
					}
				?>
			</h1>
		</div>
		<br>
		<div class="desc">
			<h3>Event Description</h3>
			<?php $sql= "SELECT TDesc from TEvents where Tno=$event_id;";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) {
						while ($row = $result->fetch_assoc()) {
				    	echo $row['TDesc'];
						}
					}
					else {
						echo 'No Result';
					}
				?>
		</div>