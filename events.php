<?php include "connect.php" ?>
<!DOCTYPE html>
<html>
<head>
	<title>Events</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="events.css">
	<link href="https://fonts.googleapis.com/css?family=Chakra+Petch" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"> 
</head>
<body class="container-fluid">
		<div style="width: 70% ;float: left;">
			<h1 class="eventsHead" style="width: 100%;">EVENTS</h1>
		</div>
		<div style="width: 30%;float: right">
			<img src="Cult.png">
		</div>
		<hr>
		<br><br>
		<br>
		<div style="width: 60%; font-size: 30%; ">
			<h1 class="eventsSubHead" style="width: 100%;">Technical Events</h1>
		</div>
		<br>
		<div class="events-div">
			<div class="events">
				<?php 
				$count_sql = 'SELECT TName from TEvents';
				$result = $conn->query($count_sql);
				$num_rows = $result->num_rows;
				for($i=1;$i<=$num_rows;$i++){
				$sql= "SELECT TName from TEvents where Tno=$i;";
					  $result = $conn->query($sql);
					if ($result->num_rows > 0) {
						while ($row = $result->fetch_assoc()) {
						echo "<a href='EventDescription.php?Tid=$i'>";
						echo $row['TName'];
						echo "</a><br>";
						}
					}
					else {
						echo 'No Result';
					}
				}
				?>
			</div>
		</div>
		<div class="events-div">
			<a href="#" class="events">EVENT</a>
		</div>
		<div class="events-div">
			<a href="#" class="events">EVENT</a>
		</div>
		<br><br>
		<br>
		<br>
		<br>
		<div style="width: 60%; font-size: 30%; ">
			<h1 class="eventsSubHead" style="width: 100%;">Cultural Events</h1>
		</div>
		<br>
		<div class="events-div">
			<a href="#" class="events">EVENT</a>
		</div>
		<div class="events-div">
			<a href="#" class="events">EVENT</a>
		</div>
		<div class="events-div">
			<a href="#" class="events">EVENT</a>
		</div>
		<br><br>
		<br>
		<br>
		<br>
		<div style="width: 60%; font-size: 30%; ">
			<h1 class="eventsSubHead" style="width: 100%;">Workshops</h1>
		</div>
		<br>
		<div class="events-div">
			<a href="#" class="events">EVENT</a>
		</div>
		<div class="events-div">
			<a href="#" class="events">EVENT</a>
		</div>
		<div class="events-div">
			<a href="#" class="events">EVENT</a>
		</div>
</body>
</html>